# Task Manager 

Training project.  
A simple console application for managing task lists.

## TECH STACK

Java Core

## SOFTWARE

* JDK 1.8
* Windows 10 64-bit

## HARDWARE

* Core i7-4790 or Ryzen 3 3200G.
* GTX 1060 6GB, GTX 1660 Super (or R9 Fury)
* 12GB RAM.
* 6GB VRAM.
* 70GB SSD storage.

## BUILD & RUN PROGRAM

```
javac ./src/ru/tsc/ichaplygina/taskmanager/*.java
cd ./src
jar cvfm ../task-manager.jar ../resources/META-INF/MANIFEST.MF ./ru/tsc/ichaplygina/taskmanager/*.class
cd ..
java -jar ./task-manager.jar
```

## DEVELOPER 

Irina Chaplygina  
Technoserv Consulting  
ichaplygina@tsconsulting.com

## SCREENSHOTS

from IDE:  
![screenshot of the application running in IDE](https://gitlab.com/irkka.hiiri/tsc-java-misc/-/raw/master/screenshots/tsc-java-02/1_run_from_ide.png)
  
.jar:  
![!screenshot of the application running from jar](https://gitlab.com/irkka.hiiri/tsc-java-misc/-/raw/master/screenshots/tsc-java-02/2_jar.png)
